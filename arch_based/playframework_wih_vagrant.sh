#!/usr/bin/env bash

# Creating base for apps on local user
mkdir -p ~/.apps/ides
mkdir -p ~/.apps/languages
mkdir -p ~/.apps/builders

# Editing .bashrc
echo "" >> ~/.bashrc
echo "#MODS" >> ~/.bashrc
echo "export TERM=xterm-color" >> ~/.bashrc
echo "APPS=\$HOME/.apps" >> ~/.bashrc
echo "IDES=\$APPS/ides" >> ~/.bashrc
echo "LANGUAGE=\$APPS/languages" >> ~/.bashrc
echo "BUILDER=\$APPS/builders" >> ~/.bashrc
echo "" >> ~/.bashrc

# Downloading and installing Java
java_url=https://download.oracle.com/otn-pub/java/jdk/8u201-b09/42970487e3af4f5aa5bca3f542482c60/jdk-8u201-linux-x64.tar.gz
#This url expires from time to time, so before executing the script check java url
#Url date: 2019/February/24

wget -q --no-check-certificate -c --header "Cookie: oraclelicense=accept-securebackup-cookie" $java_url &&
tar xzf jdk-8u201-linux-x64.tar.gz && echo "Java uncompressed" &&
mv jdk1.8.0_201 ~/.apps/languages/java &&
echo "export JAVA_HOME=\$LANGUAGE/java" >> ~/.bashrc &&
rm -rf jdk-8u201-linux-x64.tar.gz && echo "Java installed" &&

# Downloading and installing Maven
maven_url=http://ftp.man.poznan.pl/apache/maven/maven-3/3.5.4/binaries/apache-maven-3.5.4-bin.tar.gz

wget -q $maven_url &&
tar xzf apache-maven-3.5.4-bin.tar.gz && echo "Maven uncompressed" &&
mv apache-maven-3.5.4 ~/.apps/builders/maven &&
echo "export MAVEN_HOME=\$BUILDER/maven" >> ~/.bashrc &&
rm -rf apache-maven-3.5.4-bin.tar.gz && echo "Maven installed" &&

#Downloading SBT tool for playframework (Java/Scala)
sbt_url=https://piccolo.link/sbt-1.2.6.tgz

wget -q $sbt_url &&
tar xzf sbt-1.2.6.tgz && echo "Sbt uncompressed" &&
mv sbt ~/.apps/builders/sbt &&
echo "export SBT_HOME=\$BUILDER/sbt" >> ~/.bashrc &&
rm -rf sbt-1.2.6.tgz && echo "Sbt installed" &&

#Downloading node JS
node_url=https://nodejs.org/dist/v10.15.1/node-v10.15.1-linux-x64.tar.xz

wget -q $node_url &&
tar xJf node-v10.15.1-linux-x64.tar.xz && echo "NodeJS uncompressed" &&
mv node-v10.15.1-linux-x64 ~/.apps/builders/node &&
echo "export NODE_HOME=\$BUILDER/node" >> ~/.bashrc &&
rm -rf node-v10.13.0-linux-x64.tar.xz && echo "NodeJS installed" &&

#Downloading Intellij community
intellij_url=https://download.jetbrains.com/idea/ideaIC-2018.3.4.tar.gz

wget -q $intellij_url &&
tar xzf ideaIC-2018.3.4.tar.gz && echo "Intellij Community uncompressed" &&
mv idea-IC-183.5429.30 ~/.apps/ides/idea &&
rm -rf ideaIC-2018.3.4.tar.gz && echo "Intellij Community installed" &&

# Finishing PATH variable
echo "" >> ~/.bashrc &&
echo "export PATH=\$PATH:\$JAVA_HOME/bin:\$MAVEN_HOME/bin:\$SBT_HOME/bin:\$NODE_HOME/bin" >> ~/.bashrc &&

echo "Sourcing ~/.bashrc" && source ~/.bashrc && echo "Sourced ~/.bashrc" &&

echo "" >> ~/.profile
echo "export SBT_OPTS=\"\$SBT_OPTS -Dsbt.jse.engineType=Node Dsbt.jse.command=\$(which node)\"" >> ~/.profile &&

# Installing and configuring postgres
sudo pacman -Syu base-devel
sudo pacman -Syu virtualbox
yes | sudo pacman -Syu vim vagrant

#Load virtualbox modules
sudo /sbin/rcvboxdrv setup

#Installing heroku
curl https://cli-assets.heroku.com/install.sh | sh
echo "Heroku installed"

echo "Finished execution"