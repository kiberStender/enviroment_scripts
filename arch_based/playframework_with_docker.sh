#!/usr/bin/env bash

# apps directories env
apps_dir=~/.apps
ides_dir=$apps_dir/ides
languages_dir=$apps_dir/languages
builders_dir=$apps_dir/builders

# Deleting previous .apps
rm -rf $apps_dir

# Creating base for apps on local user
mkdir -p $ides_dir
mkdir -p $languages_dir
mkdir -p $builders_dir

# rc_file=~/.bashrc
rc_file=~/.zshrc
# pro_file=~/.profile
# pro_file=~/.zprofile

# Create a copy of the original file to make it easier to back it up in case of problems
cp $rc_file $rc_file.orig
cp $pro_file $pro_file.orig

# Editing rc_file
echo "" >> $rc_file
echo "# MODS" >> $rc_file
echo "export TERM=xterm-color" >> $rc_file
echo "" >> $rc_file

# Downloading and installing Java Open JDJK
java_version=14.0.2
java_tar_gz=openjdk-$java_version_linux-x64_bin.tar.gz
java_uncompressed=jdk-$java_version
java_url=https://download.java.net/java/GA/jdk$java_version/205943a0976c4ed48cb16f1043c5c647/12/GPL/$java_tar_gz

echo "Downloading Java" && wget -q --no-check-certificate -c --header "Cookie: oraclelicense=accept-securebackup-cookie" $java_url &&
echo "Downloaded Java" && tar xzf $java_tar_gz && echo "Java uncompressed" &&
mv $java_uncompressed $languages_dir/java &&
echo "export JAVA_HOME=$languages_dir/java" >> $rc_file &&
rm -rf $java_tar_gz && echo "Java installed" &&

# Downloading and installing Maven
maven_version=3.8.1
maven_tar_gz=apache-maven-$maven_version-bin.tar.gz
maven_uncompressed=apache-maven-$maven_version
maven_url=http://ftp.man.poznan.pl/apache/maven/maven-3/$maven_version/binaries/$maven_tar_gz

echo "Downloading Maven" && wget -q $maven_url && echo "Downloaded Maven" &&
tar xzf $maven_tar_gz && echo "Maven uncompressed" &&
mv $maven_uncompressed $builders_dir/maven &&
echo "export MAVEN_HOME=$builders_dir/maven" >> $rc_file &&
rm -rf $maven_tar_gz && echo "Maven installed" &&

# Downloading and installing Gradle
gradle_version=7.0
gradle_zip=gradle-$gradle_version-bin.zip
gradle_uncompressed=gradle-$gradle_version
gradle_url=https://services.gradle.org/distributions/$gradle_zip

echo "Downloading Gradle" && wget -q $gradle_url && echo "Downloaded Gradle" &&
unzip -q -a $gradle_zip && echo "Gradle uncompressed" &&
mv $gradle_uncompressed $builders_dir/gradle &&
echo "export GRADLE_HOME=$builders_dir/gradle" >> $rc_file &&
rm -rf $gradle_zip && echo "Gradle installed" &&

#Downloading SBT tool for playframework (Java/Scala)
sbt_version=1.5.1
sbt_tgz=sbt-$sbt_version.tgz
sbt_uncompressed=sbt
sbt_url=https://github.com/sbt/sbt/releases/download/v$sbt_version/$sbt_tgz # https://piccolo.link/$sbt_tgz

echo "Downloading SBT" && wget -q $sbt_url && echo "Downloaded SBT" &&
tar xzf $sbt_tgz && echo "Sbt uncompressed" &&
mv $sbt_uncompressed $builders_dir/sbt &&
echo "export SBT_HOME=$builders_dir/sbt" >> $rc_file &&
rm -rf $sbt_tgz && echo "Sbt installed" &&

#Downloading node JS
node_version=14.16.1
node_tar_xz=node-v$node_version-linux-x64.tar.xz
node_uncompressed=node-v$node_version-linux-x64
node_url=https://nodejs.org/dist/v$node_version/$node_tar_xz

echo "Downloading Node" && wget -q $node_url && echo "Downloaded Node"
tar xJf $node_tar_xz && echo "NodeJS uncompressed" &&
mv $node_uncompressed $builders_dir/node &&
echo "export NODE_HOME=$builders_dir/node" >> $rc_file &&
rm -rf $node_tar_xz && echo "NodeJS installed" &&

# Downloading Intellij
# intellij_tar_gz=ideaIC-2018.3.5.tar.gz
# intellij_uncompressed=idea-IC-183.5912.21
# intellij_url=https://download.jetbrains.com/idea/$intellij_tar_gz

# echo "Downloading Intellij" && wget -q $intellij_url && echo "Downloaded Intellij" &&
# tar xzf $intellij_tar_gz && echo "Intellij Community uncompressed" &&
# mv $intellij_uncompressed $ides_dir/idea &&
# rm -rf $intellij_tar_gz && echo "Intellij Community installed" &&

# Finishing PATH variable on rc_file
echo "" >> $rc_file &&
echo "export PATH=\$PATH:\$JAVA_HOME/bin:\$MAVEN_HOME/bin:\$GRADLE_HOME/bin:\$SBT_HOME/bin:\$NODE_HOME/bin" >> $rc_file &&

# No need for source, but I will keep it here in case I find the reason to use it
# echo "Sourcing $rc_file" && source $rc_file && echo "Sourced $rc_file" &&

# Found that this actually is worse and makes sbt fails, but I`m gonna keep it commented, maybe I`m just stupid and doing it wrong
# echo "" >> $pro_file
# echo "export SBT_OPTS=\"\$SBT_OPTS -Dsbt.jse.engineType=Node Dsbt.jse.command=$builders_dir/node\"" >> $pro_file &&

# Installing Docker and Docker-compose
sudo pacman -Syu docker vim

sudo curl -L "https://github.com/docker/compose/releases/download/1.27.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# To not use sudo with docker
sudo usermod -aG docker $USER

# Configuring git
git config --global user.name "Kleber Stender"
git config --global user.email kleber.stender@gmail.com
git config --global pull.rebase true
git config --global pull.ff only       # fast-forward only
echo "Git configured"

#Installing heroku
curl https://cli-assets.heroku.com/install.sh | sh
echo "Heroku installed"

echo "Finished execution"